# Quarantine Recipe Calculator - Cole Wilshire
Name: Cole Wilshire
Team: 2
App Title: Quarantine Recipe Calculator

Major feature:
The user is given three fields to enter an ingredient into. Pressing the "Done" button compares the user's ingredients
with the major ingredients found in recipes stored in recipes.xml. The user is then given a link to the recipe that best
fits their ingredient choices.

For the purpose of the milestone, I prepopulated the fields with ingredients to try, as I have only implemented two recipes
so far, so guessing functional ingredients would be difficult. Ideally with more recipes, it should be almost impossible to
not match any ingredients.

This feature is for the most part completely functional, however, the app currently only checks for if the contents of the
same index of two String arrays representing lists of ingredients given by the user and program match, meaning if an
ingredient is listed out of order, it does not count, so that needs to be fixed. I would also like to make the links
actually do something, like bring up the webpage they are linked to.

Minor Feature 1:
Pressing the "Favorites" button will send the user to the Favorites Activity, where they may save the latest recipe link they
received to Shared preferences. Currently only one recipe can be stored at a time, but I am planning to expand it to more
for the final app.

Minor Feature 2:
Pressing the "Profiles" button sends the user to the Profiles Activity, where they can create a Username, which is stored
in Local Preferences. Currently there is only one username allowed per device, but I plan to allow more. The app also
currently doesn't do anything with the username, other than display it in the Profiles activity. I will probably later
link it with the favorites system, so that each user has their own list of favorites.

