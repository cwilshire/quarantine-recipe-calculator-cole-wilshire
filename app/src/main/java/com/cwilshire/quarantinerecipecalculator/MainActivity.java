package com.cwilshire.quarantinerecipecalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    EditText ingredientOneEditText;
    EditText ingredientTwoEditText;
    EditText ingredientThreeEditText;
    TextView recipeLinkTextView;

    //For testing purposes only
    TextView ingredientOneTextView;
    TextView ingredientTwoTextView;
    TextView ingredientThreeTextView;
    TextView debugConsoleTextView;

    //Declare array of 3 strings to hold user's ingredients
    final int numIngredients = 4;   //For these purposes, a link also counts as an ingredient
    String[] ingredients = new String[numIngredients];

    //
    String sendRecipe = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ingredientOneEditText = findViewById(R.id.ingredientOneEditText);
        ingredientTwoEditText = findViewById(R.id.ingredientTwoEditText);
        ingredientThreeEditText = findViewById(R.id.ingredientThreeEditText);
        recipeLinkTextView = findViewById(R.id.recipeLinkTextView);

        //For testing
        ingredientOneTextView = findViewById(R.id.ingredientOneTextView);
        ingredientTwoTextView = findViewById(R.id.ingredientTwoTextView);
        ingredientThreeTextView = findViewById(R.id.ingredientThreeTextView);
        debugConsoleTextView = findViewById(R.id.debugConsoleTextView);
    }

    public void openFavorites(View view) {
        Intent intent = new Intent(this, FavoritesActivity.class);
        intent.putExtra("sentRecipe",sendRecipe);
        startActivity(intent);
    }

    public void openProfiles(View view) {
        Intent intent = new Intent(this, ProfilesActivity.class);
        startActivity(intent);
    }

    public void chooseIngredients(View view){
        //Put user-given ingredients into array
        ingredients[0] = "Filler Text";
        ingredients[1] = "tag_" + ingredientOneEditText.getText().toString().toLowerCase();
        ingredients[2] = "tag_" + ingredientTwoEditText.getText().toString().toLowerCase();
        ingredients[3] = "tag_" + ingredientThreeEditText.getText().toString().toLowerCase();

        //Arrays.sort(ingredients);

        findRecipes();
    }

    public void findRecipes(){
        //Construct a multi-dimensional array to house the contents of the recipe array in recipes.xml
        String[] recipeList = getResources().getStringArray(R.array.recipe_list);
        final int numRecipes = 25;
        final int maxIngredients = 5;
        String[][] recipeArrays = new String[numRecipes][maxIngredients];
        for (int i = 0;i < recipeList.length;i++){
            String currentRecipe = recipeList[i];
            int recipeID = getResources().getIdentifier(currentRecipe, "array", "com.cwilshire.quarantinerecipecalculator");
            String temp[] = getResources().getStringArray(recipeID);
            for (int j = 0;j<temp.length;j++){
                recipeArrays[i][j] = temp[j];
            }
        }

        //Check for matches between the user-given ingredients and recipe ingredients
        String[] threeMatchRecipes = new String[numRecipes];
        String[] twoMatchRecipes = new String[numRecipes];
        String[] oneMatchRecipes = new String[numRecipes];
        int maxMatches = 0;
        for (int i = 0;i < recipeList.length;i++){
            int numMatches = 0;
            for (int j = 0;j <= 3;j++){  //Setting j higher than 3 (4 indexes) causes a crash. So j needs to be the number of ingredients every recipe has, plus the link
                if ((ingredients[j].equals(recipeArrays[i][1])) || (ingredients[j].equals(recipeArrays[i][2])) || (ingredients[j].equals(recipeArrays[i][3]))){
                    numMatches = numMatches + 1;
                    if (numMatches > maxMatches){
                        maxMatches = numMatches;
                    }
                }
                if (numMatches >= numIngredients - 1){
                    threeMatchRecipes[i] = recipeArrays[i][0];
                }
                else if (numMatches == numIngredients - 2){
                    twoMatchRecipes[i] = recipeArrays[i][0];
                }
                else if (numMatches == numIngredients - 3){
                    oneMatchRecipes[i] = recipeArrays[i][0];
                }
            }
        }

        if (maxMatches == numIngredients - 1){
            for (int i = 0;i < threeMatchRecipes.length;i++){
                if (threeMatchRecipes[i] != null){
                    recipeLinkTextView.setText(threeMatchRecipes[i]);
                    sendRecipe = threeMatchRecipes[i];
                }
            }
        }
        else if (maxMatches == numIngredients - 2){
            for (int i = 0;i < twoMatchRecipes.length;i++){
                if (twoMatchRecipes[i] != null){
                    recipeLinkTextView.setText(twoMatchRecipes[i]);
                    sendRecipe = twoMatchRecipes[i];
                }
            }
        }
        else if (maxMatches == numIngredients - 3){
            for (int i = 0;i < oneMatchRecipes.length;i++){
                if (oneMatchRecipes[i] != null){
                    recipeLinkTextView.setText(oneMatchRecipes[i]);
                    sendRecipe = oneMatchRecipes[i];
                }
            }
        }
        else {
            recipeLinkTextView.setText("No matches found.");
        }
        //debugConsoleTextView.setText("Matches: " + Integer.toString(maxMatches));
    }
}
