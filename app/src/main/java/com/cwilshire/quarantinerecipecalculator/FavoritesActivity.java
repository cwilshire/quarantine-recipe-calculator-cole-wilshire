package com.cwilshire.quarantinerecipecalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class FavoritesActivity extends AppCompatActivity {

    TextView latestLinkTextView;
    TextView favoriteLinksTextView;

    final int numFavorites = 10;
    String[] sharedPrefNames = new String[numFavorites];
    String[] sharedPrefValues = new String[numFavorites];

    int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        latestLinkTextView = findViewById(R.id.latestLinkTextView);
        favoriteLinksTextView = findViewById(R.id.favoriteLinksTextView);
        latestLinkTextView.setText(getIntent().getExtras().getString("sentRecipe"));

        //Create list of names of shared preferences
        for(int i = 0; i < sharedPrefNames.length; i++){
            sharedPrefNames[i] = "Link " + i;
        }

        //Access first favorite, and show it in the favorites menu
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        String pullLink = sharedPref.getString(sharedPrefNames[0], null);
        favoriteLinksTextView.setText(pullLink);
    }

    public void favoriteLink(View view) {
        //Pull the number of used favorite slots from shared integers
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);

        //Pull values of items saved in Shared Preferences, and feed them into an array
        for (int i = 0; i < numFavorites; i++){
            String pullLink = sharedPref.getString(sharedPrefNames[i], null);
            sharedPrefValues[i] = pullLink;
        }

        //Save favorited link in shared preferences
        SharedPreferences.Editor editor = sharedPref.edit();
        String linkToFavorite = latestLinkTextView.getText().toString();
        //Look for an empty favorite position (null value), in which to insert a new favorite
        for (int i = 0; i < numFavorites; i++){
            if (sharedPrefValues[i] == null){
                editor.putString(sharedPrefNames[i], linkToFavorite);
                break;  //Immediately exit loop
            }
        }
        editor.apply();
    }

    public void loadFavoriteLinks(View view) {
        //Cycle through favorited links
        position++;
        if (!(position < numFavorites)){
            position = 0;
        }
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        String pullLink = sharedPref.getString(sharedPrefNames[position], null);
        while ((pullLink == null) && (position != 0)) {
            position++;
            if (!(position < numFavorites)){
                position = 0;
            }
            pullLink = sharedPref.getString(sharedPrefNames[position], null);
        }
        favoriteLinksTextView.setText(pullLink);
    }

    public void copyLink(View view) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Recipe Link", favoriteLinksTextView.getText().toString());
        clipboard.setPrimaryClip(clip);
    }
}