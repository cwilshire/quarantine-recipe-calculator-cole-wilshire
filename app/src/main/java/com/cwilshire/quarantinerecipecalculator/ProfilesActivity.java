package com.cwilshire.quarantinerecipecalculator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;

public class ProfilesActivity extends AppCompatActivity {

    TextView currentProfileTextView;
    EditText createProfileEditText;
    ImageView profilePictureImageView;
    Button takeProfilePictureButton;

    static final int TAKE_PHOTO_PERMISSION = 1;
    static final int REQUEST_TAKE_PHOTO = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiles);

        currentProfileTextView = findViewById(R.id.currentProfileTextView);
        createProfileEditText = findViewById(R.id.createProfileEditText);
        profilePictureImageView = findViewById(R.id.profilePictureImageView);
        takeProfilePictureButton = findViewById(R.id.takeProfilePictureButton);

        //Check for permissions for using camera
//        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//            takeProfilePictureButton.setEnabled(false);
//            ActivityCompat.requestPermissions(this, new String[] { android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE }, TAKE_PHOTO_PERMISSION);
//        }

        //Trigger loadProfile method
        loadProfile();

    }

    public void createProfile(View view) {
        //Save profile to shared preferences
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        String username = createProfileEditText.getText().toString();
        editor.putString("Username", username);
        editor.apply();
        loadProfile();
    }

    public void loadProfile() {
        //Load Profile Data from Shared Preferences
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        String pullUsername = sharedPref.getString("Username", null);
        currentProfileTextView.setText(pullUsername);

        //Load Profile Picture from Shared Preferences and Convert to Bitmap
        String pullProfilePicture = sharedPref.getString("Profile Picture", null);
        try {
            byte[] imageAsBytes = Base64.decode(pullProfilePicture.getBytes(), Base64.DEFAULT);
            profilePictureImageView.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
        } catch (Exception e) {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == TAKE_PHOTO_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                takeProfilePictureButton.setEnabled(true);
            }
        }
    }

    public void takeProfilePicture(View view) {
        Intent takeProfilePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takeProfilePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeProfilePictureIntent, REQUEST_TAKE_PHOTO);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO) {
            // Add here.
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                profilePictureImageView.setImageBitmap(imageBitmap);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
                byte[] b = baos.toByteArray();

                String encoded = Base64.encodeToString(b, Base64.DEFAULT);

                SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("Profile Picture", encoded);
                editor.apply();
            }
        }
    }
}
